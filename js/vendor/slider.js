window.onload = function() {
 let carousel = document.querySelector('.content-section-banner'),
  /* Carousel current image */
 cimage = 0,
  /* Carousel source images */
 csources = [
     'img/slider/slider_1.jpg',
     'img/slider/slider_2.jpg',
     'img/slider/slider_3.jpg'
 ];

  function changeCarouselImage() {
    // Reset the current image if it's ultrapassing the length - 1
    if(cimage >= 3) cimage = 0;
    // Change the source image
    carousel.style.background = 'url(' + csources[cimage ++] + ')'
    setTimeout(changeCarouselImage, 5000);
 }

 setTimeout(changeCarouselImage, 5000);
};








