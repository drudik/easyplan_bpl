let progress = document.querySelector(".calculation-process")
let complete = document.querySelector(".calculation-complete")
let printing = document.querySelector(".printing-project") 
// block-buttons
let orderNotes = document.querySelector(".btn-notes-loading")
let orderList = document.querySelector(".btn-list-loading")
let orderPrint = document.querySelector(".btn-print-project-loading")
// modal buttons
let closeTopBtn = document.querySelector(".modal-btn-close")
let closeBottomBtn = document.querySelector(".modal-btn-ok")

setTimeout(function loadingResult(){
    complete.style.visibility = "visible"
    progress.style.display = "none"
    orderNotes.classList.remove('btn-notes-loading')
    orderList.classList.remove('btn-list-loading')
    orderPrint.classList.remove('btn-print-project-loading')
    
    orderNotes.classList.add('btn-notes')
    orderList.classList.add('btn-list')
    orderPrint.classList.add('btn-print-project')
} , 5000)

orderPrint.addEventListener('click', function(){
    complete.style.display = "none"
    progress.style.display = "none"
    printing.style.display = "block"
    orderNotes.classList.remove('btn-notes')
    orderList.classList.remove('btn-list')
    orderPrint.classList.remove('btn-print-project')
    orderNotes.classList.add('btn-notes-loading')
    orderList.classList.add('btn-list-loading')
    orderPrint.classList.add('btn-print-project-loading')    
})


//open order list
orderList.addEventListener('click', function () {
    let open = document.querySelector("#modal-result")
    let modalOL = document.querySelector(".modal-window-order-list")
    open.style.visibility = "visible"
    modalOL.style.display = "block"
})

closeTopBtn.onclick = function() {
    let close = document.querySelector("#modal-result")
    let modalOL = document.querySelector(".modal-window-order-list")
    close.style.visibility = "hidden"
    modalOL.style.display = "none"
}

closeBottomBtn.onclick = function() {
    let close = document.querySelector("#modal-result")
    let modalOL = document.querySelector(".modal-window-order-list")
    close.style.visibility = "hidden"
    modalOL.style.display = "none"
}
