/*
1 - создать именнованные функции для вызова на каждой странице по отдельности.
*/


if (document.title === "room-size") {
  let roomSize = document.getElementById("btn-help-room-size")
  let closeTopBtn = document.querySelector(".modal-btn-close")
  let closeBottomBtn = document.querySelector(".modal-btn-ok")


  roomSize.onclick = function helpRoomSize() {
    let open = document.querySelector("#modal-room-size")
    open.style.visibility = "visible"
  }

  closeTopBtn.onclick = function topBtnClose() {
    let modalHid = document.querySelector("#modal-room-size")
    modalHid.style.visibility = "hidden";
  }

  closeBottomBtn.onclick = function bottomBtnClose() {
    let modalHid = document.querySelector("#modal-room-size")
    modalHid.style.visibility = "hidden";
  }
}
else if (document.title == "cold-spot") {
  let coldSpot = document.getElementById("btn-help-cold-spot")
  let closeTopBtn = document.querySelector(".modal-btn-close")
  let continueBtn = document.querySelector(".modal-btn-continue")
  let modalCSH = document.querySelector(".modal-window-cold-spot-help")

  let openModal = function() {
      let open = document.querySelector("#modal-cold-spot")
      open.style.visibility = "visible"
      modalCSH.style.visibility = "visible"
    }

  document.addEventListener("DOMContentLoaded", openModal);

  coldSpot.addEventListener('click', function() {
    let open = document.querySelector("#modal-cold-spot")
    open.style.visibility = "visible"
    modalCSH.style.visibility = "visible"
  })

  closeTopBtn.addEventListener('click', function() {
    let modalHid = document.querySelector("#modal-cold-spot")
    modalHid.style.visibility = "hidden"
    modalCSH.style.visibility = "hidden"
  })

  continueBtn.addEventListener('click', function() {
    let modalHid = document.querySelector("#modal-cold-spot")
    modalHid.style.visibility = "hidden"
    modalCSH.style.visibility = "hidden"
  })

}

else if (document.title == "floor-type") {
  let floorType = document.getElementById("btn-help-floor-type")
  let closeTopBtn = document.querySelector(".modal-btn-close")
  let closeBottomBtn = document.querySelector(".modal-btn-ok")


  floorType.onclick = function helpFloorType() {
    let open = document.querySelector("#modal-floor-type")
    open.style.visibility = "visible"
  }


  closeTopBtn.onclick = function topBtnClose() {
    let modalHid = document.querySelector("#modal-floor-type")
    modalHid.style.visibility = "hidden";
  }

  closeBottomBtn.onclick = function bottomBtnClose() {
    let modalHid = document.querySelector("#modal-floor-type")
    modalHid.style.visibility = "hidden";
  }
}
else {
  console.log("else fom help.js");
}


