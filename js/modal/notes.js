// modal buttons
let closeTop = document.querySelector(".modal-btn-close")
let closeBtn = document.querySelector(".modal-btn-close-bottom")
let notesBtn = document.querySelector(".btn-notes-loading")

//open modals orderList
notesBtn.addEventListener('click', function () {
    let open = document.querySelector("#modal-result")
    let modalN = document.querySelector(".modal-window-notes")
    open.style.visibility = "visible"
    modalN.style.display = "block"
})

closeTop.onclick = function() {
    let close = document.querySelector("#modal-result")
    let modalN = document.querySelector(".modal-window-notes")
    close.style.visibility = "hidden"
    modalN.style.display = "none"
}

closeBtn.onclick = function() {
    let close = document.querySelector("#modal-result")
    let modalN = document.querySelector(".modal-window-notes")
    close.style.visibility = "hidden"
    modalN.style.display = "none"
}

