let coldSpot = document.getElementById("btn-add-cold-spot")
let closeTopBtn = document.querySelector(".modal-btn-close-add-modal")
let modalCS = document.querySelector(".modal-window-cold-spot")
let buttonAdd = document.querySelector("#modal-cold-spot button")

coldSpot.addEventListener('click', function() {
  let open = document.querySelector("#modal-cold-spot")
  open.style.visibility = "visible";
  modalCS.style.visibility = "visible";
})

closeTopBtn.addEventListener('click', function() {
  let modalHid = document.querySelector("#modal-cold-spot")
  modalHid.style.visibility = "hidden";
  modalCS.style.visibility = "hidden"
})

buttonAdd.addEventListener('click', function() {
  let modalHid = document.querySelector("#modal-cold-spot")
  modalHid.style.visibility = "hidden";
  modalCS.style.visibility = "hidden"
})

//  disable  send form data this chunk of code need for test
const form = document.querySelector("#coldspot")
form.addEventListener('submit', (ev) => {
  ev.preventDefault()
})
